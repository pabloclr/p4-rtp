## Ejercicio 3. Análisis general
* ¿Cuántos paquetes componen la captura?:  1268
* ¿Cuánto tiempo dura la captura?: 12.81s
* Aproximadamente, ¿cuántos paquetes se envían por segundo?: 99 paquetes 
* ¿Qué protocolos de nivel de red aparecen en la captura?: IPv4
* ¿Qué protocolos de nivel de transporte aparecen en la captura?: UDP
* ¿Qué protocolos de nivel de aplicación aparecen en la captura?: RTP
## Ejercicio 4. Análisis de un paquete
* Número aleatorio elegido (N): 17
* Dirección IP de la máquina A: 192.168.0.10
* Dirección IP de la máquina B: 216.234.64.16
* Puerto UDP desde el que se envía el paquete:  49154
* Puerto UDP hacia el que se envía el paquete: 54550
* SSRC del paquete: 0x2A173650
* Tipo de datos en el paquete RTP (según indica el campo correspondiente): Payload type: ITU-T G.711 PCMU
## Ejercicio 5. Análisis de un flujo (stream)
* ¿Cuál es el primer número de tu DNI / NIE?:  0
* ¿Cuántos paquetes hay en el flujo F?: 626
* ¿El origen del flujo F es la máquina A o B?: De la maquina B( 216.234.64.16)
* ¿Cuál es el puerto UDP de destino del flujo F?: 49154
* ¿Cuántos segundos dura el flujo F?: 12.49 s
## Ejercicio 6. Métricas del flujo
* ¿Cuál es la diferencia máxima en el flujo?: 21.187 ms
* ¿Cuál es el jitter medio del flujo?: 0.22 ms
* ¿Cuántos paquetes del flujo se han perdido?: 0
* ¿Cuánto tiempo (aproximadamente) está el jitter por encima de 0.4 ms?: 0.5 s
## Ejercicio 7. Métricas de un paquete RTP
* ¿Cuál es su número de secuencia dentro del flujo RTP?: 26537
* ¿Ha llegado pronto o tarde, con respecto a cuando habría debido llegar?: Tarde, el SKEW es positivo.
* ¿Qué jitter se ha calculado para ese paquete?  5.49 ms
* ¿Qué timestamp tiene ese paquete? 1440
* ¿Por qué número hexadecimal empieza sus datos de audio? ff
## Ejercicio 8. Captura de una traza RTP
* Salida del comando `date`: dom 22 oct 2023 12:49:27 cts
* Número total de paquetes en la captura: 2672
* Duración total de la captura (en segundos): 6.5976 s
## Ejercicio 9. Análisis de la captura realizada
* ¿Cuántos paquetes RTP hay en el flujo?: 450
* ¿Cuál es el SSRC del flujo?: 0x33378a4a
* ¿En qué momento (en ms) de la traza comienza el flujo?: 2.30s
* ¿Qué número de secuencia tiene el primer paquete del flujo?: 2762635268
* ¿Cuál es el jitter medio del flujo?: 0.00 ms
* ¿Cuántos paquetes del flujo se han perdido?: 0
## Ejercicio 10 (segundo periodo). Analiza la captura de un compañero
* ¿De quién es la captura que estás usando?:
* ¿Cuántos paquetes de más o de menos tiene que la tuya? (toda la captura): 
* ¿Cuántos paquetes de más o de menos tiene el flujo RTP que el tuyo?:
* ¿Qué diferencia hay entre el número de secuencia del primer paquete de su flujo RTP con respecto al tuyo?:
* En general, ¿que diferencias encuentras entre su flujo RTP y el tuyo?:
